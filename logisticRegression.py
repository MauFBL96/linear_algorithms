import numpy as np
import pandas as pf
import tensorflow as tf
import sklearn

from sklearn.datasets import load_breast_cancer
data = load_breast_cancer()
print(type(data))
print(data.keys())

sklearn.utils.Bunch

data.keys()

data.data.shape

data.target



data.target_names

data.target.shape

data.feature_names

#Dimport data split library  
from sklearn.model_selection import  train_test_split
#aplie the dataset split train test features and targets, 
X_train , X_test, Y_train, Y_test = train_test_split(data.data,data.target,test_size = .35)
N, D = X_train.shape

#preprocessing library 
from sklearn.preprocessing import  StandardScaler
# Instace preprocesing model
scalar = StandardScaler()
# fit data
X_train = scalar.fit_transform(X_train)
X_test = scalar.transform(X_test)



# Buils the model
# input layer = input length
# dense layer = transformtion to dimension 
model = tf.keras.models.Sequential([
    tf.keras.layers.Input(shape=(D,)),
    tf.keras.layers.Dense(1, activation='sigmoid')
])

# model configue
model.compile(optimizer = 'adam',
              loss='binary_crossentropy',
              metrics = ['accuracy'])
#Train the model
r = model.fit(X_train, Y_train, validation_data= (X_test, Y_test), epochs = 100)

#Evaluate the model
print("Train score: ",model.evaluate(X_train,Y_train))
print("Test score: ",model.evaluate(X_test,Y_test))

import matplotlib.pyplot as plt
plt.plot(r.history['loss'],label ='loss')
plt.plot(r.history['val_loss'],label ='val_loss')
plt.legend()
plt.show()

plt.plot(r.history['accuracy'],label ='accuracy')
plt.plot(r.history['val_accuracy'],label ='epoch')
plt.legend()
plt.show()

P = model.predict(X_test)
print(P)

import numpy as np
P = np.round(P).flatten()
print(P)

print(Y_test)

print('manually calculated accuracy', np.mean(P==Y_test))
print('Evaluated output', model.evaluate(X_test,Y_test))

